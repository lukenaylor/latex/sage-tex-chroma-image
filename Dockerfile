FROM registry.gitlab.com/lukenaylor/latex/sagetex-image:latest

RUN mkdir /root/bin/
ENV PATH="/root/bin:${PATH}"
# Install tree-sitter by downloading the executable straight from the GitHub releases page
#  Then initialize the config for tree-sitter,
#  "/root/src" is assumed to be in the list of directories searched for parsers
RUN curl --output-dir /tmp -LO https://github.com/tree-sitter/tree-sitter/releases/download/v0.21.0/tree-sitter-linux-x64.gz \
 && gzip -d /tmp/tree-sitter-linux-x64.gz \
 && mv /tmp/tree-sitter-linux-x64 /root/bin/tree-sitter \
 && chmod +x /root/bin/tree-sitter \
 && tree-sitter init-config \
 && mkdir /root/src

# Install chroma_code by downloading the executable straight from the GitHub releases page
RUN curl --output-dir /root/bin -LO https://github.com/TomLebeda/chroma_code/releases/download/1.0.1/chromacode \
 && chmod +x /root/bin/chromacode
