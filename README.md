# SageTex Image

Container image with a full texlive, and SageMath installation
(who's `sagetex.sty` package is visible to LaTeX).
But also
[`chromacode`](https://github.com/TomLebeda/chroma_code)
installed along with dependencies.

## Download
Currently ~14GB

```
docker pull registry.gitlab.com/lukenaylor/latex/sagetex-chroma-image:latest
```

## Install parsers

Before using `chromacode` in your build process, the tree-sitter grammar for
the programming language used in your listing must be downloaded to the
`/root/src/` directory in the container.

For example, to get the [Rust parser](https://github.com/tree-sitter/tree-sitter-rust.git) (update to latest tag):

```bash
git clone --depth=1 --branch v0.20.4 https://github.com/tree-sitter/tree-sitter-rust.git /root/src/tree-sitter-rust
```

(you could add this to your Dockerfile or devcontainer config)

You can find other language parsers on the [tree-sitter website](https://tree-sitter.github.io/tree-sitter/#parsers).
